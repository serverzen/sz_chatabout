============
sz_chatabout
============

*Chatabout* is a pure javascript-based chat application.

Running
=======

After installing *Chatabout* you can run the web application with::

  pserve development.ini

Credits
=======

  * Written and maintained by Rocky Burt - rocky AT serverzen DOT com, developer
    at `ServerZen Software <http://www.serverzen.com>`_.
