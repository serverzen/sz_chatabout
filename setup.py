from setuptools import setup, find_packages

requires = [
    'pyramid>=1.4',
    'pyramid_sockjs',
    'jinja2>=2.6',
    'pyramid_jinja2>=1.5',
]

with open('README.rst') as f:
    README = f.read()

setup(name='sz_chatabout',
      version='0.1',
      description='A pure javascript-based chat application',
      long_description=README,
      classifiers=[
          "Programming Language :: Python",
          "Framework :: Pylons",
          "Topic :: Internet :: WWW/HTTP",
          "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
      ],
      author='Rocky Burt',
      author_email='rocky@serverzen.com',
      url='http://src.serverzen.com/sz_chatabout',
      keywords='web pyramid pylons',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      tests_require=requires,
      test_suite="sz_chatabout",
      entry_points={
          'paste.app_factory': [
              'app = sz_chatabout:app',
          ],
      },
      )
