from pyramid.config import Configurator


def app(global_settings, **settings):
    config = Configurator(settings=settings)

    config.include('pyramid_jinja2')
    config.add_static_view('static', 'static')

    config.include('pyramid_sockjs')

    config.scan()
    config.add_route('index', '/')

    config.include('sz_chatabout.ws')

    return config.make_wsgi_app()
