from . import ws


@ws.session()
class ChatManager(object):

    @ws.action
    def join(self, session, data):
        data['id'] = hash(session.id)
        extra = session._extra = {}
        extra['name'] = data['data']['name']
        participants = data['data']['participants'] = {}
        for v in session.manager.active_sessions():
            if not hasattr(v, '_extra'):
                continue
            participants[hash(v.id)] = v._extra['name']
        return data

    @ws.action
    def chat(self, session, data):
        data['id'] = hash(session.id)
        return data
