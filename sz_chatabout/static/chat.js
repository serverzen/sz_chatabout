define(['jquery', 'knockout', 'sockjs'],
function($, ko, SockJS) {

    var ChatContainerModel = function(manager) {
        var self = this;
        self.manager = manager;

        self.chat_entries = ko.observableArray();
        self.chat_line = ko.observable();
        self.joined = ko.observable(false);
        self.name = ko.observable();
        self.id = ko.observable();

        self.join_chat = function() {
            self.manager.join(self.name());
            self.joined(true);
        };

        self.submit_chat = function() {
            var text = self.chat_line();
            self.manager.send_chat(text);
            self.chat_line('');
        };
    };

    var PageManager = function() {
        var self = this;
        self.chat_model = new ChatContainerModel(self);
        self.users = {};

        self.sock = new SockJS(global_config.sock_url);
        self.sock.onopen = function() {
        };
        self.sock.onmessage = function(msg) {
            var obj = JSON.parse(msg.data);
            var date = new Date();
            date = date.toLocaleTimeString();
            if (obj.action == 'chat' && obj.id) {
                self.chat_model.chat_entries.unshift({
                    created: date,
                    type: 'message',
                    name: self.users[obj.id],
                    text: obj.data,
                    classes: 'message'
                });
            } else if (obj.action == 'join') {
                self.users[obj.id] = obj.data.name;
                $(obj.data.participants).each(function(idx, x) {
                    self.users[x.id] = x.name;
                });
                self.chat_model.chat_entries.unshift({
                    created: date,
                    type: 'joined',
                    name: self.users[obj.id],
                    text: 'joined',
                    classes: 'system'
                });
            }
        };
        self.sock.onclose = function() {
        };

        self.send_chat = function(text) {
            self.sock.send(JSON.stringify({action: 'chat', data: text}));
        };
        self.join = function(name) {
            self.sock.send(JSON.stringify({action: 'join', data: {name: name}}));
        }
    };

    function init() {
        if (window.chat_managers == undefined)
            window.chat_managers = [];
        var chat_manager = new PageManager();
        window.chat_managers.push(chat_manager);
        ko.applyBindings(chat_manager.chat_model);
    }

    return {
        PageManager: PageManager,
        init: init
    };
});
