requirejs_config = {
    shim: {},
    baseUrl: global_config.base_url,
    paths: {
        jquery: global_config.jquery_url,
        knockout: global_config.knockout_url,
        sockjs: global_config.sockjs_url
    },
    shim: {
        jquery: {
            exports: '$'
        }
    }
};
requirejs.config(requirejs_config);
