from pyramid.view import view_config


@view_config(route_name='index',
             renderer='sz_chatabout:templates/index.jinja2')
def index(request):
    return {}
