import json
from pyramid_sockjs.session import Session

_sessions = {}
_last_session = None


def includeme(config):
    global _sessions
    for k, v in _sessions.items():
        print v.prefix
        config.add_sockjs_route(prefix=v.prefix, session=v)


class BaseSession(Session):
    def on_message(self, message):
        m = json.loads(message)
        res = self.actions[m['action']](self.handler, self, m)
        self.manager.broadcast(json.dumps(res))


def session(name=None, prefix=None):
    class _Session(BaseSession):
        actions = {}
    _Session.name = name
    _Session.prefix = prefix

    global _sessions, _last_session
    _sessions[name] = _Session
    _last_session = _Session

    def _session(cls, _Session=_Session):
        if _Session.name is None:
            _Session.name = cls.__name__
        if _Session.prefix is None:
            _Session.prefix = '/ws/__'+_Session.name+'__'
        _Session.handler = cls()

        return cls
    return _session


def action(fn):
    _last_session.actions[fn.__name__] = fn
